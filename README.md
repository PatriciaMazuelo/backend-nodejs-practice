# Backend Nodejs practice

## NODEPOP
Aplicación para la búsqueda y publicación de anuncios de compra-venta.
La ruta de /api se llama solo api, no hay una version 1.

## Desarrollo

Para arrancar el proyecto en modo desarrollo usamos:

```sh
npm run dev
```
Para instalar las dependencias:
```sh
npm install
```

## Inicializar la base de datos

Para inicializar la base de datos usamos:

```sh
npm run installDB
```

Inicializar microservicio de creación de thumbnails

```sh 
node pusblisher.js
```


Inicializamos usuarios y anuncios.

## Rutas del API

Para obtener una lista de anuncios:

### api/ads

Para obtener una lista de tags:

### api/tags


Para filtrar por nombre, tags, precio:

### localhost:3000/$name=name
### localhost:3000/$tags=tag
### localhost:3000/$price=price  //n, -n, n-, n-m

Para crear un anuncio:

### POST localhost:3000/api

Body ==>
  name, price, sale, tags, image

Headers ==>
  token de autorización

Para logearte y conseguir un token:

### POST localhost:3000/api/login

Body ==>
  email, password











