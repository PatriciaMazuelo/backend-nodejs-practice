var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const session = require("express-session");
const MongoStore = require("connect-mongo");
const utils = require("./lib/utils");
const ImageController = require("./controllers/imageController");
const LoginController = require("./controllers/loginController");
const sessionAuth = require("./lib/sessionAuthMiddleware");
const jwtAuth = require("./lib/jwtAuthMiddleware");

//const basicAuth = require("./lib/basicAuthMiddleware");
//const multer = require("multer");
//const upload = multer({ dest: "./public/images" });

//Connection to the data base
require("./lib/connectMongoose");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "html");
app.engine("html", require("ejs").__express);

app.locals.title = "NodePOP";

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

const loginController = new LoginController();
const imageController = new ImageController();

//Rutas de mi API
app.use("/api", require("./routes/api/ads"))
//app.use("/api/ads", imageController.post, require("./routes/api/ads"));
app.use("/api/tags", require("./routes/api/tags"));
app.post("/api/login", loginController.postJWT);
//app.post("/api/ads/:image", jwtAuth, imageController.post);
//la digo a la petición POST que me guarde la imagen en el storage de multer
// app.post(
//   "/api/ads/image",
//   jwtAuth,
//   uploadFile,
//   imageController.post
//   //   // req.file is the name of your file in the form above, here 'uploaded_file'
//   //   // req.body will hold the text fields, if there were any
//   //console.log(req.file, req.body);
// );


// Setup de i18n
const i18n = require("./lib/i18nConfigure");
app.use(i18n.init);

//Setup de las sessiones de la APP
app.use(
  session({
    name: "nodepop-session",
    secret: "C2=2]Apw`PbSn*7)`UfrLPELHhd%f",
    saveUninitialized: true,
    resave: false,
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 2, // 2 dias de inactividad
    },
    store: MongoStore.create({
      mongoUrl: process.env.MONGODB_CONNECTION_STRING,
    }),
  })
);

app.use((req, res, next) => {
  res.locals.session = req.session;

  next();
});

//Rutas de mi APP

app.use("/", require("./routes/index"));
app.use("/private", sessionAuth, require("./routes/private"));
app.use("/users", require("./routes/users"));
app.use("/change-locale", require("./routes/change-locale"));
//lo hacemos con get para facilitar test unitarios
app.get("/login", loginController.index);
app.post("/login", loginController.post);
app.get("/logout", loginController.logout);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  res.status(err.status || 500);

  if (utils.isAPIRequest(req)) {
    res.json({ error: err.message });
    return; // para que no se ejecute lo siguiente y entonces no responder dos veces
  }
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page

  res.render("error");
});

module.exports = app;
