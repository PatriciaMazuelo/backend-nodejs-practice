const mongoose = require("mongoose");
  
const adSchema = new mongoose.Schema({
  name: String,
  sell: Boolean,
  price: Number,
  image: String,
  tags: [String]
  
  });

  adSchema.statics.lista = function(filtro, skip, limit, select, sort) {
    const query = Ad.find(filtro); 
    query.skip(skip);
    query.limit(limit);
    query.select(select);
    query.sort(sort);
    return query.exec();
  }

  
  
const Ad = mongoose.model("Ad", adSchema)

module.exports = Ad;