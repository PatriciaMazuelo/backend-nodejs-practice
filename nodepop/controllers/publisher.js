//const imageResicer = require("../lib/jimpResize");
const { Responder } = require("cote");
const Jimp = require("jimp");

const responder = new Responder({ name: "publisher" });

responder.on("thumbnail-creater", (req, done) => {
  const { image } = req;

  const filePath = __dirname
    .split("\\")
    .filter((dir) => dir !== "controllers")
    .join("\\");
  console.log(__dirname); //'C:\\Users\\patri\\OneDrive\\Bureaublad\\backend-nodejs-practice\\nodepop\\controllers
  console.log(filePath); //'C:\\Users\\patri\\OneDrive\\Bureaublad\\backend-nodejs-practice\\nodepop
  Jimp.read(`${filePath}\\public\\images\\${image}`)
    .then((img) => {
      img.resize(100, 100);
      img.write(`${filePath}\\public\\thumbnails\\${image}`);
    })
    .catch((err) => {
      console.log(err);
    });
  done(image);
});
