"use strict";

const jwt = require("jsonwebtoken");
const User = require("../models/User");

class LoginController {
  index(req, res, next) {
    res.locals.error = " ";
    res.render("login");
  }

  async post(req, res, next) {
    try {
      const { email, password } = req.body;

      //busco el user en la bbdd
      const user = await User.findOne({ email });
      console.log(user);

      //si falla o no encuentra los credenciales le muestro un error
      if (!user || !(await user.comparePassword(password))) {
        res.locals.error = res.__("Invalid credentials");
        res.render("login");
        return;
      }

      //si está lo redirijo a la zona privada
      req.session.logedUser = {
        _id: user._id,
      };

      res.redirect("/private");
    } catch (err) {
      next(err);
    }
  }

  logout(req, res, next) {
    req.session.regenerate((err) => {
      if (err) {
        next(err);
        return;
      }
      res.redirect("/");
    });
  }

  async postJWT(req, res, next) {
    try {
      const { email, password } = req.body;

      const user = await User.findOne({ email });
      console.log(user);

      if (!user || !(await user.comparePassword(password))) {
        res.json({ error: "Invalid credentials" });
        return;
      }

      // creo un JWT si es valido con el _id del usuario dentro
      jwt.sign(
        { _id: user._id },
        process.env.JWT_SECRET,
        { expiresIn: "2d" },
        (err, jwtToken) => {
          if (err) {
            next(err);
            return;
          }
          //devuelvo al cliente el token generado
          res.json({ token: jwtToken });
        }
      );
    } catch (err) {
      next();
    }
  }
}

module.exports = LoginController;
