"use strict";

const { Requester } = require("cote");

const requester = new Requester({ name: "nodepop-image-controller" });

//clase para controlar el microservicio que crea thumbnails y los reajusta
class ImageController {
  //POST /api
  post(req, res, next) {
    try {
      const image = req.file.filename;
      console.log(image);
      requester.send(
        {
          type: "thumbnail-creater",
          image,
        },
        (result) => {
          res.json({ result: result });
          return;
        }
      );
    } catch (err) {
      next(err);
    }
  }
}

module.exports = ImageController;
