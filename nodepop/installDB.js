'use strict';

require("dotenv").config()
const dbConnection = require('./lib/connectMongoose');


const Ad = require('./models/Ad');
const User = require("./models/User");
const adData = require('./ads.json');

main().catch(err => console.log('There was an error', err));

async function main() {
  await initAds();
  await initUser();

  dbConnection.close();
}

async function initAds() {
  // elimino todos los documentos de la colección de anuncios
  const deleted = await Ad.deleteMany();
  console.log(`Deleted ${deleted.deletedCount} ads.`);

  // crear anuncios iniciales
  const ads = await Ad.insertMany(adData.ads);
  console.log(`Created ${ads.length} ads.`);


}

async function initUser(){
  const deleted = await User.deleteMany();
  console.log(`Deleted ${deleted.deletedCount} users`);

  const result = await User.insertMany([
    {
      email: "admin@example.com",
      password: await User.hashPassword("1234"),
    },
    {
      email: "user1@example.com",
      password: await User.hashPassword("1234"),
    }
  ]);

  console.log(`Insertados ${result.length} usuarios.`);
}