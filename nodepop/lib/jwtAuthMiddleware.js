'use strict';

const jwt = require('jsonwebtoken');

// modulo que devuelve un middleware

module.exports = (req, res, next) => {
  // recojo el jwtToken de la cabecera (o de otros sitios)
  const jwtToken = req.get('Authorization') || req.query.token || req.body.token;
  
  // compruebo que tengo el token
  if (!jwtToken) {
    const error = new Error('no token provided');
    error.status = 401;
    next(error);
    return;
  }

  // compruebo que el token es válido
  jwt.verify(jwtToken, process.env.JWT_SECRET, (err, payload) => {
    if (err) {
      err.message = 'invalid token';
      err.status = 401;
      next(err);
      return;
    }

    
    req.apiAuthUserId = payload._id;

    // si es valido, continuar
    next();
    
  });

};