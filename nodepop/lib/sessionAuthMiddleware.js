'use strict';

module.exports = (req, res, next) => {
  if (!req.session.logedUser) {
    res.redirect('/login');
    return;
  }

  next();

}