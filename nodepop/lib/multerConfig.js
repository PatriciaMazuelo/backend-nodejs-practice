"use strict";

//configuro multer para que me guarde la imagen en el destino y la nombre
const multer = require("multer");
//const path = require("path");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/images");
  },

  filename: (req, file, cb) => {
    cb(null, file.fieldname + Date.now() + file.originalname);
  },
});

const uploadFile = multer({ storage }).single("image");


module.exports = uploadFile;
