const express = require('express');
const router = express.Router();

/** GET /private */
router.get('/', (req, res, next) => {
  res.render('private');
});

module.exports = router;