var express = require('express');
var router = express.Router();
const Ad = require("../models/Ad");

/* GET home page. */
router.get('/', async function(req, res, next) {
  
    try{

      const name = req.query.name;
      const price = req.query.price;
      const tags = req.query.tags;
      const skip = parseInt(req.query.skip);
      const limit = parseInt(req.query.limit);
      const select = req.query.select; 
      const sort = req.query.sort;
      

      const filter = {};

      if(name){
          filter.name = name;  
      }

      if(price){

        var priceRange = price.split('-');

        if(priceRange[0] === '' & priceRange[1] !== ''){
          filter.price = { $lte: priceRange[1]}

        } else if(priceRange[0] !== '' & priceRange[1] === ''){
              filter.price = { $gte: priceRange[0]}

        } else if(priceRange[0] !== '' & priceRange[1] !== ''){
              filter.price = { $gte: priceRange[0], $lte: priceRange[1]}

        } else if(price === price){
          filter.price = price;
        } 
      }

      if(tags){
          filter.tags = tags;
      }
      
      
      const ads = await Ad.lista(filter, skip, limit, select, sort);
      res.render('index', { title: 'NODEPOP', ads: ads});

    }catch (err){
        next(err)
    }

  
});

router.post("/", async (req, res, next) =>{
  try{

      const adData = {
        ...req.body,
        image: req.file.path,
      };
      

      const ad = new Ad(adData);

      const createdAd = await ad.save();

      

      res.status(201).json({ result: createdAd });

  } catch (err){
      next(err);
  }
});






module.exports = router;
