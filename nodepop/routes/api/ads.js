const express = require("express");
const Ad = require("../../models/Ad");
const jwtAuth = require("../../lib/jwtAuthMiddleware");
const uploadFile = require("../../lib/multerConfig");
const ImageController = require("../../controllers/imageController");

const imageController = new ImageController();


const router = express.Router();


// /api/ads
//Lista de anuncios
router.get("/ads", jwtAuth, async (req, res, next) =>{
    try{

        const name = req.query.name;
        const price = req.query.price;
        const tag = req.query.tag;
        const skip = parseInt(req.query.skip);
        const limit = parseInt(req.query.limit);
        const select = req.query.select; // campos
        const sort = req.query.sort;
        

        const filter = {};
        if(name){
            filter.name = name;
        }
        if(price){
            filter.price = price;
        }
        if(tag){
            filter.tag = tag;
        }

        const ads = await Ad.lista(filter, skip, limit, select, sort);
        res.json({ results: ads });
    }catch (err){
        next(err)
    }
});

// /api/ads:id
//metodo para obtener un anuncio
router.get("/:identificador", async (req, res, next) =>{
    try{
        const _id = req.params.identificador;

        const ad = await Ad.find({_id: _id});
        res.json({ result: ad });
    }catch(err){
        next (err);
    }
});

//const path = require("path");
//POST /api/ads (body)
//Crear un anuncio
router.post("/", uploadFile, imageController.post, async (req, res, next) =>{
    
    try{

        const adData = {
          ...req.body,
          image: req.file.path,
        };
        

        const ad = new Ad(adData);

        const createdAd = await ad.save();

        res.status(201).json({ result: createdAd });

    } catch (err){
        next(err);
    }
});

//DELETE /api/ads:id
//Elimina un anuncio
router.delete("/:id", async (req, res, next) =>{
    try{

        const _id = req.params.id;

        await Ad.deleteOne({ _id: _id});
        res.json();

    }catch(err){
        next(err)
    }
});

//PUT /api/ads:id (body)lo que quiero actualizar
//Actualizar un anuncio
router.put("/:id", async (req, res, next) =>{
    try{

        const _id = req.params.id;
        const adData = req.body;

        const updatedAd = await Ad.findOneAndUpdate({ _id: _id}, adData, {
            new: true
        });

        if(!updatedAd){
            res.status(404).json({error: "not found"});
            return;
        }
        res.json({ result: updatedAd });

    }catch(err){
        next(err)
    }
})


module.exports = router;