const express = require("express");
const Ad = require("../../models/Ad");

const router = express.Router();

router.get("/", async (req, res, next) =>{
    try{
        
        
        //const tags = req.query.tags;
        const showTags = await Ad.find().distinct("tags");
        res.json({ results: showTags });

    }catch(err){
      next(err);
    }
});

module.exports = router;